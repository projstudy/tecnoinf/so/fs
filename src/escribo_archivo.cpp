// Created by e974616 on 10/30/2018.
//
#include "fs.h"
#include <cstring>
#include <iostream>
#include <sys/shm.h>

using namespace std;

void writeToFile(int fileInode, string data, sc_fs *fs) {
  size_t dataSize = data.size();
  size_t qtyFreeSpace = freeSpace(fileInode, fs);
  int pointers = fs->inodes[fileInode].qtyPointers;
  int lastBlock = fs->inodes[fileInode].blocks[pointers - 1];
  // Entra en el archivo o se pasa del limite?
  if (qtyFreeSpace >= dataSize) {
    // Calculo inodos necesarios
    size_t spaceLeftInLastBlock =
        BLOCKSIZE - 1 - strlen(fs->blocks[lastBlock].data);
    // El epacio que queda, alcanza?
    if (dataSize > spaceLeftInLastBlock) {
      // Cuanto espacio extra preciso?
      size_t extraSpaceNeeded = dataSize - spaceLeftInLastBlock;
      // Calculo la cantidad de bloques necesarios para el espacio extra
      size_t blocksNeeded = (extraSpaceNeeded + BLOCKSIZE - 1) / BLOCKSIZE;
      // quedan bloques libres?
      if (FSSIZE >= fs->usedBlocks + blocksNeeded) {
        // empizo a escribir en el ultimo block
        writeToBlock(lastBlock, data.substr(0, spaceLeftInLastBlock), fs);
        data = data.substr(spaceLeftInLastBlock, data.size());
        // muevo index de la substring
        while (data.size() > 0) {
          // A medida que pido, escribo
          int newBlock;
          pop(fs->freeBlocks, newBlock);
          // guardo el block en el inodo
          fs->inodes[fileInode].blocks[pointers] = newBlock;
          // incremento los punteros del inodo
          fs->inodes[fileInode].qtyPointers++;
          // incremento los blocks usados
          fs->usedBlocks++;
          // si la cantidad restante, es masyor que un bloque entero?
          pointers++;
          if (data.size() >= BLOCKSIZE) {
            writeToBlock(newBlock, data.substr(0, BLOCKSIZE - 1), fs);
            // desplazo el index
            data = data.substr(BLOCKSIZE - 1, data.size());
          } else {
            writeToBlock(newBlock, data, fs);
            data = "";
          }
        }
      } else {
        cout << "No hay espacio en el disco." << endl;
      }
    } else {
      // empizo a escribir en el ultimo block
      int pointer = fs->inodes[fileInode].qtyPointers;
      int block = fs->inodes[fileInode].blocks[pointer - 1];
      writeToBlock(block, data, fs);
    }
  } else {
    cout << "El contenido es muy grande para el archivo." << endl;
  }
}

int main(int argc, char **argv) {
  if (argc > 2) {
    // clave para la memory
    string path = argv[1];
    vector<string> tokens;
    if (isPathValid(path)) {
      tokens = splitPath(path);
      // Hay mas de 1 token?
      if (tokens.size() > 0) {
        string disk = "/tmp/";
        disk += tokens.front();
        // quito el disco de los tokens
        tokens.erase(tokens.begin());
        auto fs = attachDisk(disk.c_str());
        // me pude conectar al fs?
        if (fs) {
          // quedan mas tokens?
          if (tokens.size() == 0) {
            cout << "/ es un directorio" << endl;
          } else {
            int parentInode;
            // Existe el fullPath que escribio?
            if (pathExists(tokens, fs, parentInode)) {
              // nombre de lo que quiere modificar
              string file = tokens.back();
              int fileInode;
              if (existsFile(file, parentInode, fs, fileInode)) {
                if (fs->inodes[fileInode].type == DIR) {
                  cout << file << " es un directorio." << endl;
                } else {
                  writeToFile(fileInode, argv[2], fs);
                }
              } else {
                cout << "No existe el archivo." << endl;
              }
            } else {
              cout << "No existe el path." << endl;
            }
          }
          shmdt(fs);
        } else {
          cout << "No se puede acceder al disco." << endl;
        }
      } else {
        cout << "Ingrese un path y texto, ej: /home/file1 \"texto a ingresar\""
             << endl;
      }
    } else {
      cout << "Ingrese un path y texto, ej: /home/file1 \"texto a ingresar\""
           << endl;
    }
  } else {
    cout << "Ingrese un path y texto, ej: /home/file1 \"texto a ingresar\""
         << endl;
  }
  return 0;
}
