// Created by e974616 on 10/30/2018.
//
#include "fs.h"
#include <cstring>
#include <iostream>
#include <string>
#include <sys/shm.h>

void rmFile(const string &file, int parentInode, sc_fs *fs) {
  int totalPointers = fs->inodes[parentInode].qtyPointers;
  int currentPointer = 0;
  int targetInode = -1;
  int parentBlock = 1;
  string blockData;
  while (currentPointer < totalPointers && targetInode < 0) {
    parentBlock = fs->inodes[parentInode].blocks[currentPointer];
    blockData = fs->blocks[parentBlock].data;
    targetInode = findInode(file, blockData);
    currentPointer++;
  }
  push(fs->freeInodes, targetInode);
  fs->usedInodes--;
  for (int i = 0; i < fs->inodes[targetInode].qtyPointers; ++i) {
    int targetBlock = fs->inodes[targetInode].blocks[i];
    push(fs->freeBlocks, targetBlock);
    // fs->blocks[targetBlock].isFree = true;
    fs->usedBlocks--;
    strcpy(fs->blocks[targetBlock].data, "");
  }
  string toDelete = file;
  toDelete += ":";
  toDelete += to_string(targetInode);
  toDelete += ":";
  size_t startPosition = blockData.find(toDelete);
  blockData.erase(startPosition, toDelete.size());
  strcpy(fs->blocks[parentBlock].data, blockData.c_str());
};

int main(int argc, char **argv) {
  if (argc > 1) {
    // clave para la memory
    string fullPath = argv[1];
    vector<string> tokens;
    if (isPathValid(fullPath)) {
      tokens = splitPath(fullPath);
      // hay mas de 1 token?
      if (tokens.size() > 0) {
        string disk = "/tmp/";
        disk += tokens.front();
        // quito el disco de los tokens
        tokens.erase(tokens.begin());
        auto fs = attachDisk(disk.c_str());
        // me pude conectar al fs?
        if (fs) {
          // quedan mas tokens?
          if (tokens.size() == 0) {
            cout << "Ingrese el path del archivo a eliminar ej: /disco1/file"
                 << endl;
          } else {
            int parentInode;
            // Existe el fullPath que escribio?
            if (pathExists(tokens, fs, parentInode)) {
              string file = tokens.back();
              int targetInode;
              if (existsFile(file, parentInode, fs, targetInode)) {
                if (fs->inodes[targetInode].type == DIR) {
                  cout << "No es un archivo." << endl;
                } else {
                  rmFile(file, parentInode, fs);
                }
              } else {
                cout << "No existe el archivo." << endl;
              }
            } else {
              cout << "No existe el path" << endl;
            }
          }
          shmdt(fs);
        } else {
          cout << "No se puede acceder al disco." << endl;
        }
      } else {
        cout << "Ingrese el path del archivo a eliminar ej: /disco1/file"
             << endl;
      }
    } else {
      cout << "Ingrese el path del archivo a eliminar ej: /disco1/file" << endl;
    }
  } else {
    cout << "Ingrese un path, ej: /disco1/home" << endl;
  }
  return 0;
}
