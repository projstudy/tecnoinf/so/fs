// Created by e974616 on 10/30/2018.
//
#include "fs.h"
#include <cstring>
#include <iostream>
#include <map>
#include <regex>
#include <string>
#include <sys/shm.h>
#include <unistd.h>

using namespace std;

// Separa el path en varios tokens
vector<string> splitPath(string fullPath) {
  // Vector de strings
  vector<string> tokens;
  // guardo el path en stringstream
  stringstream path(fullPath);
  // string temporal
  string tmp;
  while (getline(path, tmp, '/')) {
    if (tmp.size() > 0) {
      tokens.push_back(tmp);
    }
  }
  return tokens;
}

// agrega un inodo/block al stack
bool push(sc_stack &stack, int value) {
  bool result;
  if (stack.front == stack.end && stack.current > 0) {
    result = false;
  } else {
    stack.values[stack.end] = value;
    stack.end = (stack.end + 1) % FSSIZE;
    stack.current++;
    result = true;
  }
  return result;
}

// quita un inodo/block del stack
bool pop(sc_stack &stack, int &value) {
  bool result;
  if (stack.current == 0) {
    value = -1;
    result = false;
  } else {
    value = stack.values[stack.front];
    stack.front = (stack.front + 1) % FSSIZE;
    stack.current--;
    result = true;
  }
  return result;
}

// verifica que el string del path sea valido mediante regex
bool isPathValid(string &path) {
  // Regex que comprueba path
  bool result = regex_match(path, regex("^[/]([^/]+[/])*([^/]*)$"));
  // Si encuentro un : el regex es falso
  result = path.find(':') == string::npos;

  if (result) {
    // Si el path es valido, elimino el ultimo slash, si lo tiene.
    if (path.back() == '/') {
      path.pop_back();
    }
  }
  return result;
}

// verifica que la string no tenga mas de 12 char
bool isStringValid(const string &dirName) {
  if (dirName.length() >= 13) {
    cout << "Nombre de dir demasiado largo" << endl;
    return false;
  } else {
    return true;
  }
};

// El string no puede contener :
bool isStringSafe(const string &dirName) {
  bool result = false;
  if (dirName.find(':') != string::npos) {
    cout << "Nombre no valido, no puede contener ':'." << endl;
  } else {
    result = true;
  }
  return result;
};

// Pre: el texto tiene que entrar en block
// Post: append text to block
void writeToBlock(int block, string text, sc_fs *fs) {
  char *s = strdup(fs->blocks[block].data);
  strcat(s, text.c_str());
  strcpy(fs->blocks[block].data, s);
}

// retorna la cantidad de espacio libre en un block
size_t freeSpace(int inode, sc_fs *fs) {
  // el espacio maximo posible es que tenga 4 blocks
  size_t totalSpace = (BLOCKSIZE - 1) * 4;
  int totalPointers = fs->inodes[inode].qtyPointers;
  size_t ocup = 0;
  for (int i = 0; i < totalPointers; ++i) {
    int block = fs->inodes[inode].blocks[i];
    ocup += strlen(fs->blocks[block].data);
  }
  return totalSpace - ocup;
}

// retorna el numero de block en el cual se debe escribir el dato
// si retorna -1 si no le pudo asignar block
int assignParentBlock(string data, int parentInode, sc_fs *fs) {
  /*
   * Busco si entra en uno de los blocks que ya tiene asignado
   */
  int finalBlock = -1;
  int currentPointer = 0;
  int totalPointers = fs->inodes[parentInode].qtyPointers;
  while (currentPointer < totalPointers && finalBlock < 0) {
    int tmpBlock = fs->inodes[parentInode].blocks[currentPointer];
    if (strlen(fs->blocks[tmpBlock].data) + data.length() <= BLOCKSIZE) {
      finalBlock = tmpBlock;
    } else {
      currentPointer++;
    }
  }
  /*
   *  Si no entro en ninguno, veo si le puedo asignar otro bloque
   * */
  if (finalBlock < 0 && totalPointers < 4) {
    if (pop(fs->freeBlocks, finalBlock)) {
      // Actualizo la cantidad de punteros del nodo
      strcpy(fs->blocks[finalBlock].data, ":");
      fs->inodes[parentInode].qtyPointers++;
      fs->usedBlocks++;
      // La cantidad de blockes usados se increment
      fs->inodes[parentInode].blocks[totalPointers] = finalBlock;
    }
  }
  return finalBlock;
};

// verifica si un string se encuentra en los blocks de un directorio
bool existsFile(string file, int dirInode, sc_fs *fs, int &inode) {
  inode = -1;
  int totalPointers = fs->inodes[dirInode].qtyPointers;
  int currentPointer = 0;
  while (currentPointer < totalPointers && inode < 0) {
    int block = fs->inodes[dirInode].blocks[currentPointer];
    inode = findInode(file, fs->blocks[block].data);
    currentPointer++;
  }
  return inode != -1;
}

// verifica si cada token del vector existe
// devuelve en parentInode el ultimo inodo encontrado
bool pathExists(vector<string> splitPath, sc_fs *fs, int &parentInode) {
  // comienzo por la raiz
  parentInode = 0;
  // busco todos los tokens menos el ultimo
  int elements = splitPath.size();
  elements--;
  int i = 0;
  // busco mientras me queden tokens y mientras exista (!=-1)el inodo
  while (i < elements && parentInode >= 0) {
    string path = splitPath[i];
    int totalPointers = fs->inodes[parentInode].qtyPointers;
    int currentPointer = 0;
    // Busco si existe un inodo para el siguiente path, si es -1 el path no
    // es correcot.
    int found = -1;
    while (currentPointer < totalPointers && found < 0) {
      int block = fs->inodes[parentInode].blocks[currentPointer];
      found = findInode(path.c_str(), fs->blocks[block].data);
      currentPointer++;
    }
    // guardo el resultado en el parentInodo
    parentInode = found;
    i++;
  }
  // Si el parent Inodo existe, verifico que sea un dir
  if (parentInode != -1) {
    if (fs->inodes[parentInode].type != DIR) {
      // si no es dir, devuelvo path no valido
      parentInode = -1;
    }
  }
  return parentInode != -1;
};

// Dado un nombre de archivo, busca si existe en el block
// y retorna su numeor de inodo.
// Si no existe, retorna -1
int findInode(string dir, string blockData) {
  int result = -1;
  dir.insert(0, ":");
  dir += ":";
  size_t index = blockData.find(dir);
  if (index != string::npos) {
    index += dir.size();
    size_t iterator = index;
    bool stop = false;
    while (iterator < blockData.size() && !stop) {
      if (blockData.at(iterator) == ':') {
        stop = true;
      } else {
        iterator++;
      }
    }
    result = stoi(blockData.substr(index, iterator - index));
  }
  return result;
}

// escribe el nuevo archivo/dir en un bloque padre.
void createDirFile(const string &data, int parentBlock, int newInode,
                   int newBlock, InodeType type, sc_fs *fs) {
  // Marco como usados el bloque e inodo pedidos.
  fs->usedInodes++;
  fs->usedBlocks++;
  strcat(fs->blocks[parentBlock].data, data.c_str());
  // inicializo el inodo hijo.
  fs->inodes[newInode].type = type;
  fs->inodes[newInode].qtyPointers = 1;
  fs->inodes[newInode].blocks[0] = newBlock;
  if (type == DIR) {
    strcpy(fs->blocks[newBlock].data, ":");
  }
};

sc_fs *attachDisk(const char *disk) {
  key_t key = ftok(disk, 25);
  // creo la memoria compartida
  int shmid = shmget(key, sizeof(sc_fs), 0);
  if (shmid > 0 && key > 0) {
    // me apego a la memoria
    auto *fs = (sc_fs *)shmat(shmid, nullptr, 0);
    return fs;
  } else {
    return nullptr;
  }
}
