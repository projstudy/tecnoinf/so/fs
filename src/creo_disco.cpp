// Created by e974616 on 10/30/2018.
//
#include "fs.h"
#include <cstring>
#include <fcntl.h>
#include <iostream>
#include <string>
#include <sys/shm.h>

void createDisk(string str) {
  // clave para la memory
  string path = "/tmp/";
  path += str;
  if (-1 != open(path.c_str(), O_CREAT, 0777)) {
    key_t key = ftok(path.c_str(), 25);
    // creo la memoria compartida
    int shmid = shmget(key, sizeof(sc_fs), 0);
    if (shmid > -1) {
      cout << "Ya existe el disco: /" << str << endl;
    } else {
      int shmid = shmget(key, sizeof(sc_fs), 0666 | IPC_CREAT);
      if (shmid > -1) {
        // me apego a la memoria
        auto fs = (sc_fs *)shmat(shmid, nullptr, 0);
        // Inode 0 con bloque 0
        fs->inodes[0].type = DIR;
        fs->inodes[0].blocks[0] = 0;
        fs->inodes[0].qtyPointers = 1;
        fs->usedInodes = 1;
        fs->usedBlocks = 1;
        strcpy(fs->blocks[0].data, ":");
        for (int i = 1; i < FSSIZE; ++i) {
          push(fs->freeInodes, i);
          push(fs->freeBlocks, i);
        }
        // detach
        shmdt(fs);
        cout << "Disco " << str << " creado" << endl;
      } else {
        cout << "No se pudo crear el disco" << endl;
      }
    }
  } else {
    perror("Error");
  }
}

int main(int argc, char **argv) {
  if (argc > 1) {
    string path = argv[1];
    if (isStringValid(path)) {
      char *dir = strtok(argv[1], "'/");
      if (dir != NULL && strlen(dir) < 8) {
        createDisk(dir);
      } else {
        cout << "El nombre: " << dir << " no es valido" << endl;
      }
    } else {
      cout << "El nombre: " << path << " no es valido" << endl;
    }
  } else {
    cout << "Ingrese nombre de disco, ej: '/disco1'" << endl;
  }
  return 0;
}
