// Created by e974616 on 10/30/2018.
//
#include "fs.h"
#include <cstring>
#include <iostream>
#include <string>
#include <sys/shm.h>

using namespace std;

int main(int argc, char **argv) {
  if (argc > 1) {
    string path = argv[1];
    if (isStringValid(path)) {
      // clave para la memory
      char *str = strdup(argv[1]);
      char *pch = str;
      pch = strtok(pch, "/");
      string disk = "/tmp/";
      if (pch != NULL) {
        disk += pch;
        key_t key = ftok(disk.c_str(), 25);
        // creo la memoria compartida
        int shmid = shmget(key, sizeof(sc_fs), 0);
        if (shmid < 0) {
          cout << "No se puede acceder al disco." << endl;
        } else {
          int rtn = shmctl(shmid, IPC_RMID, NULL);
          if (rtn < 0) {
            cout << "El disco no pudo ser eliminado" << endl;
          } else {
            cout << "Disco eliminado" << endl;
          }
        }
      } else {
        cout << "Se debe indiciar el disco duro, ej: /disco1" << endl;
      }
      free(str);
    } else {
      cout << "Se debe indiciar el disco duro, ej: /disco1" << endl;
    }
  } else {
    cout << "Se debe indiciar el disco duro, ej: /disco1" << endl;
  }
  return 0;
}
