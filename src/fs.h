//
//#include <stdc++.h>
#include <iostream>
#include <string>
#include <vector>

#ifndef FS_FS_H
#define FS_FS_H

#define FSSIZE 1024
#define BLOCKSIZE 512

using namespace std;
enum InodeType { DIR, DOC };

struct sc_block {
  char data[BLOCKSIZE];
};

struct sc_inode {
  InodeType type;
  int blocks[4];
  int qtyPointers;
};

struct sc_stack {
  int values[FSSIZE];
  int end;
  int front;
  int current;
};

struct sc_fs {
  sc_block blocks[FSSIZE];
  sc_inode inodes[FSSIZE];
  sc_stack freeInodes;
  sc_stack freeBlocks;
  int usedInodes;
  int usedBlocks;
};

vector<string> splitPath(string fullPath);

bool push(sc_stack &stack, int value);

bool pop(sc_stack &stack, int &value);

bool isPathValid(string &path);

bool isStringValid(const string &dirName);

bool isStringSafe(const string &dirName);

int assignParentBlock(string data, int parentInode, sc_fs *fs);

bool pathExists(vector<string> splitPath, sc_fs *fs, int &parentInode);

int findInode(string dir, string blockData);

void createDirFile(const string &data, int parentBlock, int newInode,
                   int newBlock, InodeType type, sc_fs *fs);

bool existsFile(string file, int dirInode, sc_fs *fs, int &inode);

bool isDir(string dir, int dirInode, sc_fs *fs, int &childInode);

void writeToBlock(int block, string text, sc_fs *fs);

size_t freeSpace(int inode, sc_fs *fs);

sc_fs *attachDisk(const char *disk);

#endif // FS_FS_H
