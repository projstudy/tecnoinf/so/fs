// Created by e974616 on 10/30/2018.
//
#include "fs.h"
#include <cstring>
#include <iostream>
#include <string>
#include <sys/shm.h>

int main(int argc, char **argv) {
  if (argc > 1) {
    // clave para la memory
    string fullPath = argv[1];
    vector<string> tokens;
    if (isPathValid(fullPath)) {
      tokens = splitPath(fullPath);
      // Hay mas de 1 token?
      if (tokens.size() > 0) {
        string disk = "/tmp/";
        disk += tokens.front();
        // quito el disco de los tokens
        tokens.erase(tokens.begin());
        cout << disk << endl;
        auto fs = attachDisk(disk.c_str());
        // me pude conectar al fs?
        if (fs) {
          // quedan mas tokens?
          if (tokens.size() > 0) {
            // Hay suficientes inodos y bloques?
            if (fs->usedBlocks < FSSIZE && fs->usedInodes < FSSIZE) {
              int parentInode;
              // Existe el fullPath que escribio?
              if (pathExists(tokens, fs, parentInode)) {
                // nombre de lo que quiere crear
                string dir = tokens.back();
                if (isStringValid(dir)) {
                  int fileInode;
                  // verifico que no exista un archivo igual en el directorio
                  if (!existsFile(dir, parentInode, fs, fileInode)) {
                    // pido bloque e inodo nuevo
                    int newBlock;
                    int newInode;
                    pop(fs->freeBlocks, newBlock);
                    pop(fs->freeInodes, newInode);
                    // preparo el string que voy a guardar
                    string data = dir;
                    data.append(":");
                    data += to_string(newInode);
                    data.append(":");
                    // Hay lugar en el directorio?
                    size_t free = freeSpace(parentInode, fs);
                    if (free >= data.size()) {
                      int parentBlock =
                          assignParentBlock(data, parentInode, fs);
                      // Enonctre un bloque con espacio en el dir?
                      if (parentBlock > -1) {
                        createDirFile(data, parentBlock, newInode, newBlock,
                                      DIR, fs);
                      } else {
                        // devuelvo los blocks e inodes
                        push(fs->freeBlocks, newBlock);
                        push(fs->freeInodes, newInode);
                        cout << "Disco lleno." << endl;
                      }
                    } else {
                      // devuelvo los blocks e inodes
                      push(fs->freeBlocks, newBlock);
                      push(fs->freeInodes, newInode);
                      cout << "Directorio lleno." << endl;
                    }
                  } else {
                    cout << "Ya existe el archivo." << endl;
                  }
                } else {
                  cout << "Nombre no valido. Max 12 chars sin ':'." << endl;
                }
              } else {
                cout << "No existe el path." << endl;
              }
            } else {
              cout << "Disco lleno." << endl;
            }
          } else {
            cout << "Debe especificar un path valido. Ej: /disco1/home" << endl;
          }
          shmdt(fs);
        } else {
          perror("Error: \n");
          cout << "No se puede acceder al disco." << endl;
        }
      } else {
        cout << "Debe especificar un path valido. Ej: /disco1/home" << endl;
      }
    } else {
      cout << "Debe especificar un path valido. Ej: /disco1/home" << endl;
    }
  } else {
    cout << "Debe especificar un path valido. Ej: /disco1/home" << endl;
  }
  return 0;
}
