// Created by e974616 on 10/30/2018.
//
#include "fs.h"
#include <cstring>
#include <iostream>
#include <string>
#include <sys/shm.h>

void printFile(int fileInode, sc_fs *fs) {
  int totalPointers = fs->inodes[fileInode].qtyPointers;
  for (int i = 0; i < totalPointers; ++i) {
    int block = fs->inodes[fileInode].blocks[i];
    if (i + 1 == totalPointers) {
      cout << fs->blocks[block].data << endl;
    } else {
      cout << fs->blocks[block].data;
    }
  }
};

int main(int argc, char **argv) {
  if (argc > 1) {
    string path = argv[1];
    vector<string> tokens;
    if (isPathValid(path)) {
      tokens = splitPath(path);
      if (tokens.size() > 0) {
        // Si solo indico disco, es error...
        string disk = "/tmp/";
        disk += tokens.front();
        tokens.erase(tokens.begin());
        auto fs = attachDisk(disk.c_str());
        // me pude conectar al fs?
        if (fs) {
          if (tokens.size() == 0) {
            cout << "/ es un directorio" << endl;
          } else {
            int parentInode;
            // esxiste el path?
            if (pathExists(tokens, fs, parentInode)) {
              string last = tokens.back();
              // Si es un directorio, no se mostrar
              int childInode;
              if (existsFile(last, parentInode, fs, childInode)) {
                if (fs->inodes[childInode].type == DIR) {
                  cout << last << " es un directorio." << endl;
                } else {
                  printFile(childInode, fs);
                }
              } else {
                cout << "No existe el archivo." << endl;
              }
            } else {
              cout << "No existe el path." << endl;
            }
          }
          shmdt(fs);
        } else {
          cout << "No se puede acceder al disco." << endl;
        }
      } else {
        cout << "Ingrese el path que desea imprimir ej: /disco1/home" << endl;
      }
    } else {
      cout << "Ingrese un path, ej: /disco1/home" << endl;
    }
  } else {
    cout << "Ingrese un path, ej: /disco1/home" << endl;
  }
  return 0;
}
