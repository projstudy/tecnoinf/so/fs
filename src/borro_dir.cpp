// Created by e974616 on 10/30/2018.
//
#include "fs.h"
#include <cstring>
#include <iostream>
#include <sys/ipc.h>
#include <sys/shm.h>

bool rmDir(const string &file, int parentInode, sc_fs *fs) {
  bool result = true;
  // obtengo el inodo y block de donde lo tengo que borrar
  int totalPointers = fs->inodes[parentInode].qtyPointers;
  int currentPointer = 0;
  int targetInode = -1;
  int parentBlock = 1;
  string blockData;
  while (currentPointer < totalPointers && targetInode < 0) {
    parentBlock = fs->inodes[parentInode].blocks[currentPointer];
    blockData = fs->blocks[parentBlock].data;
    targetInode = findInode(file, blockData);
    currentPointer++;
  }
  // Me fijo que el dir no tenga contenido
  totalPointers = fs->inodes[targetInode].qtyPointers;
  currentPointer = 0;
  while (currentPointer < totalPointers && result) {
    int aux = fs->inodes[targetInode].blocks[currentPointer];
    if (strlen(fs->blocks[aux].data) > 1) {
      result = false;
    }
    currentPointer++;
  }
  // Si el dir esta vacio, lo elimino
  if (result) {
    push(fs->freeInodes, targetInode);
    // stack magic
    // fs->inodes[targetInode].isFree = true;
    fs->usedInodes--;
    for (int i = 0; i < fs->inodes[targetInode].qtyPointers; ++i) {
      int targetBlock = fs->inodes[targetInode].blocks[i];
      push(fs->freeBlocks, targetBlock);
      // stack magic
      // fs->blocks[targetBlock].isFree = true;
      strcpy(fs->blocks[targetBlock].data, "");
      fs->usedBlocks--;
    }
    string toDelete = file;
    toDelete += ":";
    toDelete += to_string(targetInode);
    toDelete += ":";
    size_t startPosition = blockData.find(toDelete);
    blockData.erase(startPosition, toDelete.size());
    strcpy(fs->blocks[parentBlock].data, blockData.c_str());
  }
  return result;
};

int main(int argc, char **argv) {
  if (argc > 1) {
    // clave para la memory
    string path = argv[1];
    vector<string> tokens;
    if (isPathValid(path)) {
      // Hay mas de 1 token?
      if (tokens.size() > 0) {
        string disk = "/tmp/";
        disk += tokens.front();
        // quito el disco de los tokens
        tokens.erase(tokens.begin());
        auto fs = attachDisk(disk.c_str());
        // me pude conectar al fs?
        if (fs) {
          // quedan mas tokens?
          if (tokens.size() == 0) {
            cout << "Ingrese el path del archivo a eliminar ej: /disco1/file"
                 << endl;
          } else {
            int parentInode;
            if (pathExists(tokens, fs, parentInode)) {
              string dir = tokens.back();
              int dirInode;
              if (existsFile(dir, parentInode, fs, dirInode)) {
                if (fs->inodes[dirInode].type == DIR) {
                  if (!rmDir(dir, parentInode, fs)) {
                    cout << "El directorio no esta vacio" << endl;
                  }
                } else {
                  cout << "No es un directorio." << endl;
                }
              } else {
                cout << "No existe el archivo." << endl;
              }
            } else {
              cout << "No existe el path" << endl;
            }
          }
          shmdt(fs);
        } else {
          cout << "No se puede acceder al disco." << endl;
        }
      } else {
        cout << "Ingrese el path del archivo a eliminar ej: /disco1/file"
             << endl;
      }
    } else {
      cout << "Ingrese el path del archivo a eliminar ej: /disco1/file" << endl;
    }
  } else {
    cout << "Ingrese un path, ej: /disco1/home" << endl;
  }
  return 0;
}
