// Created by e974616 on 10/30/2018.
//
#include "fs.h"
#include <cstring>
#include <iostream>
#include <string>
#include <sys/shm.h>

string printBlock(char *blockData, sc_fs *fs) {
  string final;
  /* get the first token */
  char *data = new char[strlen(blockData)];
  strcpy(data, blockData);
  char *path = strtok(data, ":");
  /* walk through other tokens */
  while (path != NULL) {
    // El siguiente corte es el inodeone
    string tmp = path;
    path = strtok(NULL, ":");
    int inode = stoi(path);
    if (fs->inodes[inode].type == DIR) {
      final += "D - ";
    } else {
      final += "F - ";
    }
    final += tmp;
    final += "\n";
    path = strtok(NULL, ":");
  }
  delete[] data;
  return final;
}

void printDir(int dirInode, sc_fs *fs) {
  int totalPointers = fs->inodes[dirInode].qtyPointers;
  for (int i = 0; i < totalPointers; ++i) {
    int block = fs->inodes[dirInode].blocks[i];
    cout << printBlock(fs->blocks[block].data, fs);
  }
};

int main(int argc, char **argv) {
  if (argc > 1) {
    string path = argv[1];
    vector<string> tokens;
    if (isPathValid(path)) {
      tokens = splitPath(path);
      if (tokens.size() > 0) {
        // Si solo indico disco, es error...
        string disk = "/tmp/";
        disk += tokens.front();
        tokens.erase(tokens.begin());
        auto fs = attachDisk(disk.c_str());
        // me pude conectar al fs?
        if (fs) {
          // verifico si escribio algo mas que el disco
          if (tokens.size() == 0) {
            printDir(0, fs);
          } else {
            int parentInode;
            if (pathExists(tokens, fs, parentInode)) {
              string last = tokens.back();
              int dirInode;
              if (existsFile(last, parentInode, fs, dirInode)) {
                if (fs->inodes[dirInode].type == DIR) {
                  printDir(dirInode, fs);
                } else {
                  cout << "\nF- " << last << endl;
                }
              } else {
                cout << "No existe el archivo." << endl;
              }
            } else {
              cout << "No existe el path" << endl;
            }
          }
          shmdt(fs);
        } else {
          cout << "No se puede acceder al disco." << endl;
        }
      } else {
        cout << "Ingrese el path que desea imprimir ej: /disco1/home" << endl;
      }
    } else {
      cout << "Ingrese un path, ej: /disco1/home" << endl;
    }
  } else {
    cout << "Ingrese un path, ej: /disco1/home" << endl;
  }
  return 0;
}
