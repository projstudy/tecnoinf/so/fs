# Proyecto Sistemas Operativos

## Implementar un FS estilo unix en memoria
### Letra

Se solicita implementar un sistema de archivos virtual tipo UFS (Unix File System) en memoria.
El disco virtual consta de 1024 bloques de datos, donde cada bloque tiene 512 bytes. El tamaño
máximo de un archivo es de 4 bloques. El nombre de un archivo tiene como máximo 8 caracteres y
3 de extensión.

El contenido de los archivos comunes sera array de caracteres, de cuyo largo depende de la
cantidad de bloques que este utilice, respetando siempre el limite de 4 bloques.
Deberá haber una estructura de directorios como la de Unix y cada archivo deberá pertenecer
a un directorio.

El disco se debe llamar igual que un directorio de 1er nivel (por ejemplo, ‘/disco1’)
Se deben implementar las operaciones de:

* creo_disco: crea un disco (ya formateado).
* creo_dir: crea un directorio.
* borro_dir: borra un directorio.
* creo_archivo: crea un archivo.
* escribo_archivo: agrega un texto al final de un archivo.
* leo_archivo, lee un archivo (lo muestra en pantalla).
* muestra_dir: muestra por pantalla el contenido de un directorio, indicando cuales son directorios y cuales archivos comunes.
* borra_archivo: borra un archivo.
* borro_disco: eliminar el disco.

## Implementacion

Core:
```cpp 
struct sc_fs {
    sc_block blocks[FSSIZE]; //1024
    sc_inode inodes[FSSIZE]; //1024
    sc_stack freeBlocks;
    sc_stack freeInodes;
    int usedInodes;
    int usedBlocks;
};

struct sc_inode {
    InodeType type;
    int qtyBlocks;
    int blocks[4]; //cantidad de blocks por inode
};

struct sc_block {
    char data[BLOCKSIZE]; //512
};

enum InodeType {
   DIR, DOC  //FILE esta en uso
};

struct sc_stack {
    int values[FSSIZE];
    int current;
    int end;
    int front;
};

```

Cada inodo puede tener un maximo de 4 blocks.

Los datos en los blocks se guardan como texto plano, en caso de que el inodo
sea un directorio; la tabla de contenido se guarda de la siguiente forma:

`:home:1:file:3:doc.txt:23:`

Donde el primer campo representa el nombre del archivo/directorio contenido, y
el segundo campo representa el numero de inodo que le corresponde.

El stack FIFO (cola) se utiliza con la idea de conseguir los bloques e inodos libres sin la
necesidad de realizar un loop en busca de uno no ocupado.

Se utiliza **FIFO** sobre LIFO porque si un bloque se libera, la idea es que el sistema lo
vuelva asignar como ultima opcíon. Porque en caso de que se quiera
recuperar el bloque eliminado, seria posible.

## Como compilar

```
    git clone https://github.com/forbi/fs
    cd fs/src
    make
```

Al compilar, se createl directorio build con los binarios necesarios.
